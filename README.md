zsl-desktop-common
=========

Ansible роля за общи настройки на работните станции.

Requirements
------------

N/A

Role Variables
--------------

Засега няма default променливи, които да могат да се променят.

Dependencies
------------

N/A

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
