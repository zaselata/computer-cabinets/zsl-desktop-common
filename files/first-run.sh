#!/bin/bash

function new_hostname() {
  rnd_string=$(pwgen -snv1 10)
  echo "ZSL-${rnd_string^^}"
}

if [ "$(/sbin/dmidecode -s system-manufacturer)" == "QEMU" ]; then
  echo "$(date) | Still running in a VM, skipping..." >> /var/lib/zsl-first-run/firstrun.log
  exit 0
fi

sleep 20

if [ $(hostname) == "zsl-desktop" ]; then
  echo "$(date) | Setting new hostname..." >> /var/lib/zsl-first-run/firstrun.log
  n_host=$(new_hostname)
  hostnamectl set-hostname $n_host --static
  sed -i "s/zsl-desktop/$n_host/g" /tmp/foo
else
  echo "$(date) | Skipping setting new hostname. It's already set... " >> /var/lib/zsl-first-run/firstrun.log
fi

wget -q --spider http://google.com
if [ $? -eq 0 ]; then
  echo "$(date) | Generating yggdrasil config..." > /var/lib/zsl-first-run/firstrun.log
  yggdrasil -normaliseconf -useconffile /etc/yggdrasil.conf.zsl > /etc/yggdrasil.conf
  touch /var/lib/zsl-first-run/firstrunfile
  /sbin/reboot
else
  echo "$(date) | Skipping Yggdrasil config until internet connection is available..." >> /var/lib/zsl-first-run/firstrun.log
fi
